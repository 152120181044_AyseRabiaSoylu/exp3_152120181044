/*
* Circle.cpp
*
*  Created on: 8 Kas 2018
*      Author: ykartal
*/


#include "Circle.h"

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double R) {
	this->r = R;
}

void Circle::setR(int R) {
	this->r = R;
}

double Circle::getR()const {
	return r;
}

double Circle::calculateCircumference()const {
	return PI * r * 2;
}

double Circle::calculateArea()const {
	return PI * r * r;
}

bool Circle::equals(double R) {
	if (r == R) return true;
	else return false;
}
