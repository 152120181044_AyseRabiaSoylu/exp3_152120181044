/*
* Triangle.cpp
*
*  Created on: 8 Kas 2018
*      Author: ykartal
*/

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double A) {
	a = A;
}

void Triangle::setB(double B) {
	b = B;
}

void Triangle::setC(double C) {
	c = C;
}

double Triangle::calculateCircumference() {
	return a + b + c;
}


