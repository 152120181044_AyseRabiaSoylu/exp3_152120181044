/*
* Square.cpp
*
*  Created on: 8 Kas 2018
*      Author: ykartal
*/

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double A) {
	a = A;
	if (a != b)setB(A);
}

void Square::setB(double B) {
	b = B;
	if (b != a)setA(B);
}

double Square::calculateCircumference() {
	return (a + b) * 2;
}

double Square::calculateArea() {
	return a * b;
}
